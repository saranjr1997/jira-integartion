#!/usr/bin/env python3

import aws_cdk as cdk

from jira_integration.jira_integration_stack import JiraIntegrationStack


app = cdk.App()
JiraIntegrationStack(app, "jira-integration")

app.synth()
