from distutils import core
from pickle import TRUE
from typing_extensions import runtime
from unicodedata import name
from constructs import Construct
from aws_cdk import ( 
    Duration,
    Stack,
    aws_iam as iam,
    aws_sqs as sqs,
    aws_sns as sns,
    aws_sns_subscriptions as subs,
    aws_lambda as lambda_
)


class JiraIntegrationStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        queue = sqs.Queue(
            self, "JiraIntegrationQueue",
            visibility_timeout=Duration.seconds(300),queue_name="JiraIntegrationQueue.fifo"            
        )


        gitlab_lambda = lambda_.Function(self, "gitlab_integartion",        
            runtime=lambda_.Runtime.PYTHON_3_8,
            code=lambda_.AssetCode("lambdas/gitlab_lambda"),
            handler="gitlab_lambda.lambda_handler",
            function_name="Gitlab-integartion",            
            # tracing=lambda_.Tracing.ACTIVE,
            memory_size=128
            )
        

        jira_lambda = lambda_.Function(self, "Jira_integartion",        
            runtime=lambda_.Runtime.PYTHON_3_8,
            code=lambda_.AssetCode("lambdas/jira_lambda"),
            handler="lambda_function.lambda_handler",
            function_name="Jira-integartion",            
            # tracing=lambda_.Tracing.ACTIVE,
            memory_size=128
            )        
        

        jira_layer = lambda_.LayerVersion(self,"jira_layer",code=lambda_.AssetCode("layers/jira.zip"),
                                        layer_version_name="JiraLayer",
                                        compatible_runtimes=[lambda_.Runtime.PYTHON_3_8,lambda_.Runtime.PYTHON_3_9])

        pymysql_layer = lambda_.LayerVersion(self,"pymysql_layer",code=lambda_.AssetCode("layers/pymysql.zip"),
                                        layer_version_name="pymysqlLayer",
                                        compatible_runtimes=[lambda_.Runtime.PYTHON_3_8,lambda_.Runtime.PYTHON_3_9])

        jira_lambda.add_layers(jira_layer,pymysql_layer)
        gitlab_lambda.add_layers(jira_layer,pymysql_layer)



