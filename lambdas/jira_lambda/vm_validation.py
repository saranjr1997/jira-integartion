from jira import JIRA

class VMTicket:

    REQUIRED_VM_FIELDS = ['Summary', 'Description' , 'Issue Type', 'Security Severity', 'Team Owner', 'Vulnerability Source', 'Vulnerability Type', 'Asset', 'Project']

    def __init__(self, client):
        self.client = client
        self.__map_fields__()

    def __map_fields__(self):
        # {"<Capitalized Human Field Name>": "<Jira Field Name>", ... }
        self.field_name_map = {field['name']:field['id'] for field in self.client.fields()}
    
    def get_open_issue(self, project, summary, severity, asset):

        # add input validation here

        jql = f'project={project} and summary~"{summary}" and "Security Severity[Dropdown]"="{severity}" and Asset~"{asset}" and status != "Remediated" and status != "Risk Accepted"'
        print(jql)
        return self.client.search_issues(jql)
    
    def create_issue(self, fields):

        if not all(f in fields and fields[f] for f in self.REQUIRED_VM_FIELDS):
            raise ValueError(f'All VM required fields must be populated: {self.REQUIRED_VM_FIELDS}')

        issue_dict = {}
        print(fields)
        for field in fields:
            print(field)
            issue_dict[self.field_name_map[field]] = fields[field]
        
        project, summary, severity, asset = (issue_dict[self.field_name_map['Project']],
        issue_dict[self.field_name_map['Summary']],
        issue_dict[self.field_name_map['Security Severity']]['value'],
        issue_dict[self.field_name_map['Asset']])
        
        print(f'existing_issues{project}')
        print(f'existing_issues{summary}')
        print(f'existing_issues{severity}')
        print(f'existing_issues{asset}')
        existing_issues = self.get_open_issue(project, summary, severity, asset)
        print(f'existing_issues{existing_issues}')

        if existing_issues:
            print(f"Won't create issue \"{summary}\" with severity {severity} for asset {asset} in project {project} since such tickets already exist:")

            for issue in existing_issues:
                print(f'http://go/jira/{issue.key}')

        else:
            return self.client.create_issue(fields = issue_dict)