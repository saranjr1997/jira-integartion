import json, os
from jira import JIRA
from aws_lambda_powertools import Logger
import boto3
from vm_validation import VMTicket
from datetime import datetime, timedelta
from database import DatabaseManager

jira_endpoint = os.environ['JiraEndpoint']
jira_username = os.environ['JiraUserName']
jira_token = os.environ['JiraToken']
logger = Logger(service="Jira-integration")

db_manager = DatabaseManager()
# MYSQL Database connection and Execute select query from database
db_manager.database_connection()

def lambda_handler(event, context):
    
    logger.debug({"event": event})
    
    
    # conigure Jira client
    jira_client = JIRA(server=jira_endpoint, basic_auth=(jira_username, jira_token))
    vm_ticket = VMTicket(jira_client)
    
    for msg in event["Records"]:
        logger.debug({'response from SQS':json.loads(msg["body"])})
        issue = json.loads(msg["body"])
        Vulnerability_id = msg['attributes']['MessageGroupId']
        logger.debug({'response from SQS Vulnerability_id':Vulnerability_id})
        try:
            issue = vm_ticket.create_issue(issue)
            logger.info({"Jira card created successfully": issue})
            db_manager.insert_data_gitlab(Vulnerability_id)
        except Exception as e:
            logger.error({"Error while calling jira issue creation": e})