import json, os
import requests
import boto3
from aws_lambda_powertools import Logger
import gitlab_query
from gitlab_issue_creation import GitlabIssueCreation
from database import DatabaseManager
from sqs_handler import SQSManager
logger = Logger(service="gitlab-lambda")

# Geeting gitlab endpoint and token details from environment variable
endpoint = os.environ['GitlabEndpoint']
token = os.environ['Token']
report_type = os.environ['ReportType']
project_name = os.environ['ProjectName']

# Initialising the class object as a global variable
db_manager = DatabaseManager()
issueobject = GitlabIssueCreation()
sqs_manager = SQSManager()

# MYSQL Database connection and Execute select query from database
db_manager.database_connection()
db_manager.select_data("Jira_Fields")
db_manager.select_data("Gitlab_id")
db_manager.close_dbcon()

def lambda_handler(event, context):
  headers = {
      'Authorization': token
      }
  endCursor = ""
  hasNextPage = "true"
  
  # Call Gitlab endpoint till end of the page
  # while True :
  query=gitlab_query.graphql(endCursor,report_type,project_name)
  logger.debug({"query data is": query})
  try:
    response = requests.post(endpoint, json={"query": query}, headers=headers)
    if response.status_code == 200 and ("errors" not in response):
      logger.debug({"Query successfull": response})
    else:
      logger.error({"Query failed to run with status code ": response.status_code})
      logger.error({"Query failed to run with error ": (response.json())["errors"][0]["message"]})
      # break
  except Exception as e:
    logger.error({"exception occured": e})
  # break
  
  json_object = response.json()
  logger.debug({"json Object is ": json_object})
  
  #Calculate hasNextPage parameter to get last page and break the service on last page
  hasNextPage = json_object['data']['project']['vulnerabilities']['pageInfo']['hasNextPage']
  hasNextPage = str(hasNextPage)
  logger.debug({"hasNextPage ": hasNextPage})
  # if hasNextPage == "False" :
  #   logger.info("Loop Breaked as we get last page from gitlab response")
  #   break
  
  # Store the endcursor to read the next page
  endCursor = json_object['data']['project']['vulnerabilities']['pageInfo']['endCursor']
  logger.debug({"endCursor ": endCursor})
  
  if json_object["data"]["project"]["vulnerabilities"]["nodes"] == [] :
    logger.error("Loop Breaked as we get empty node response")
    # break
  else:
    json_object_array = json_object["data"]["project"]["vulnerabilities"]["nodes"]
    logger.info({"jsonObject": json_object_array})
    
    # Call Issue Creation for formatting the response
    issueobject.jira_issue_fields(json_object_array,db_manager,sqs_manager)

  return {"body":"Successful", "status":200}