import json
from jira import JIRA
from aws_lambda_powertools import Logger
from datetime import datetime  
from datetime import timedelta
from database import DatabaseManager
import functools


logger = Logger(service="Jira-integration")
jira_project = 'VMDEV'

class GitlabIssueCreation:
    
    def __init__(self):
        logger.debug({"gitlab issue creation init"})
    
    def jira_issue_fields(self, json_array, db_manager,sqs_manager):
        
        logger.debug(json_array)
        # Iterating all the vulnarabilities in a loop and create Issue response
        for json_object in json_array:
            severity_details = self.severity_mapping(json_object['severity'].lower())
            security_severity = severity_details[0]
            logger.debug({"security_severity  ": security_severity})
            Vulnerability_id = json_object['id'].split('/')[-1]
            
            
            due_date = severity_details[1]
            
            # Getting asset value based on report type
            if json_object['reportType'] == 'DAST':
                asset = self.asset_mapping(json_object['location']['hostname'],json_object['reportType'])
            else: 
                asset = self.asset_mapping(json_object['location']['file'], json_object['reportType'])
            logger.debug({"asset  ": asset})
            
            # calling the database module
            db_response = db_manager.getdata_from_cache("Jira_Fields",asset)
            logger.debug({"db_response  ": db_response})
            
            db_response_Vulnerability_id = db_manager.getdata_from_cache("Gitlab_id",Vulnerability_id)
            logger.debug({"db_response  ": db_response_Vulnerability_id})
            if db_response_Vulnerability_id != {}:
                logger.debug("Issue already created")
                return
            
            # Mapping databse response in issue data
            vulnerability_type = db_response['Vulnerability_type']
            team_owner = db_response['Owner']
            assignee = db_response['Assignee']
            lables = db_response['Lables']
            
            # Create description 
            detailed_description = self.description_mapping(json_object)
            
            issue = {'Summary':json_object['title'] +"-"+Vulnerability_id, 
                                'Description':detailed_description , 
                                'Issue Type': {'name': 'Story'}, 
                                'Security Severity': {'value': security_severity}, 
                                'Team Owner': {'value': team_owner }, 
                                'Vulnerability Source': {'value': 'Gitlab' }, 
                                'Vulnerability Type': {'value': vulnerability_type}, 
                                'Asset': asset,
                                'Due date' : due_date,
                                'Project': jira_project,
                                'Assignee' : {'value': [assignee]},
                                'Labels' : [lables]
            }
            
            # Call SQS handler for sending message in SQS queue                  
            sqsresponse = sqs_manager.send_msg(issue, Vulnerability_id)
            logger.debug({"response from sqs queue": sqsresponse})
    
    # Method for creating customize description
    def description_mapping(self, git_json_object):
        
        severity_details = self.severity_mapping(git_json_object['severity'].lower())
        Vulnerability_id = git_json_object['id'].split('/')[-1]
        
        if git_json_object['reportType'] == 'DAST':
            location_des = 'Location : ' +  git_json_object['location']['hostname'] + git_json_object['location']['path']
        else: 
            location_des = 'Location : ' +  'https://gitlab.com' + git_json_object['vulnerabilityPath']
        logger.debug({"location_des  ": location_des})
        
        security_severity_des = 'Security Severity : ' +  severity_details[0]
        tool_des = 'Tool : ' + git_json_object['reportType']
        scanner_des = 'Scanner : ' + git_json_object['scanner']['name']
        identifiers_des0 =  'identifiers : ' 
        
        identifiers_des = ''
        for identifiers in git_json_object['identifiers']:
            if "url" in identifiers :
                logger.debug({"identifiers['url']  ": identifiers['url']})
                if identifiers['url']== None :
                    identifiers_des = identifiers_des + identifiers['name'] + "\n" 
                else:
                    identifiers_des = identifiers_des + identifiers['name'] + " : " + identifiers['url'] + "\n"
            else:
                identifiers_des = identifiers_des + identifiers['name'] + "\n" 
        
        description = git_json_object['description']     
        if git_json_object['description'] == None :
            logger.debug({"Empty description": git_json_object['description'] })
            description = ""
        
        vulnerability_des = "Issue created from vulnerability : " + '[' +Vulnerability_id + ":|" +'https://gitlab.com'+git_json_object['vulnerabilityPath']+"]"
                
        logger.debug({"identifiers_des is  ": identifiers_des})
        detailed_description = vulnerability_des  + "\n\n" + description + '\n' + security_severity_des + "\n" + tool_des + "\n" + scanner_des + "\n" + location_des  + "\n" + identifiers_des0 + "\n"  + identifiers_des 
        logger.debug({"Detailed Description  ": detailed_description}) 
        return detailed_description

        
    # Method for severity mapping and populating due_date 
    def severity_mapping(self, argument):
        switcher = {
            "critical": ["Critical", (datetime.now() + timedelta(days=14)).strftime("%Y-%m-%d")],
            "high": ["High", (datetime.now() + timedelta(days=30)).strftime("%Y-%m-%d")],
            "medium": ["Medium", (datetime.now() + timedelta(days=60)).strftime("%Y-%m-%d")],
            "low" : ["Low",(datetime.now() + timedelta(days=180)).strftime("%Y-%m-%d")],
            "info" : ["Info",(datetime.now() + timedelta(days=100)).strftime("%Y-%m-%d")],
            "none" : ["None",(datetime.now() + timedelta(days=14)).strftime("%Y-%m-%d")] }
        
        logger.debug({"switcher  ": switcher})
        return switcher.get(argument, "None")
    
    # Asset mapping based on report type
    def asset_mapping(self, argument,reportType):
        logger.debug({"asset_mapping argument  ": argument})
        logger.debug({"asset_mapping reportType  ": reportType})
        file = argument.split('/')
        if reportType == "DAST":
            asset = file[2]
        else :
            asset = file[0]
            
        logger.debug({"Asset  ": asset})
        return asset
        