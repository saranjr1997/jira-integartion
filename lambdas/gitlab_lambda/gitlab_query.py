def graphql(endCursor,report_type,project_name ):
    if report_type == "SAST":
      query = """{
      project(fullPath: "%s") {
        vulnerabilities(reportType: SAST, after :"", first : 1) {
          pageInfo{
          hasNextPage
          startCursor
          endCursor
          }
          nodes {
            location { ... on VulnerabilityLocationSast { blobPath, file } }
            description
            confirmedAt
            confirmedBy {
              id
            }
            #pageInfo
            description
            descriptionHtml
            #details
            detectedAt
            discussions {
              edges {
                node {
                  id
                }
              }
            }
            dismissedAt
            dismissedBy {
              id
            }
            externalIssueLinks {
              edges {
                node {
                  id
                }
              }
            }
            falsePositive
            hasSolutions
            id
            identifiers {
              externalId
              externalType
              name
              url
            }
            links {
              name
            }
            mergeRequest {
              id
            }
            message
            notes {
              edges {
                node {
                  id
                }
              }
            }
            primaryIdentifier {
              externalId
              externalType
              name
              url
            }
            project {
              id
            }
            reportType
            resolvedAt
            resolvedBy {
              id
            }
            resolvedOnDefaultBranch
            scanner {
              id
              name
            }
            severity
            state
            title
            userNotesCount
            userPermissions {
              adminVulnerabilityIssueLink
            }
            vulnerabilityPath
            id
          }
        }
      }}""" % (project_name)
      # % (project_name, endCursor)
    elif report_type == "DAST":
      query = """{
      project(fullPath: "%s") {
        vulnerabilities(reportType: DAST, after :"", first : 1) {
          pageInfo{
          hasNextPage
          startCursor
          endCursor
          }
          nodes {
            location { ... on VulnerabilityLocationDast { path,param,requestMethod,hostname }}
            description
            confirmedAt
            confirmedBy {
              id
            }
            #pageInfo
            description
            descriptionHtml
            #details
            detectedAt
            discussions {
              edges {
                node {
                  id
                }
              }
            }
            dismissedAt
            dismissedBy {
              id
            }
            externalIssueLinks {
              edges {
                node {
                  id
                }
              }
            }
            falsePositive
            hasSolutions
            id
            identifiers {
              externalId
              externalType
              name
              url
            }
            links {
              name
            }
            mergeRequest {
              id
            }
            message
            notes {
              edges {
                node {
                  id
                }
              }
            }
            primaryIdentifier {
              externalId
              externalType
              name
              url
            }
            project {
              id
            }
            reportType
            resolvedAt
            resolvedBy {
              id
            }
            resolvedOnDefaultBranch
            scanner {
              id
              name
            }
            severity
            state
            title
            userNotesCount
            userPermissions {
              adminVulnerabilityIssueLink
            }
            vulnerabilityPath
            id
          }
        }
      }}""" % (project_name)
      # % (project_name, endCursor)
    elif report_type == "SECRET_DETECTION":
      query = """{
      project(fullPath: "%s") {
        vulnerabilities(reportType: SECRET_DETECTION, after :"", first : 1) {
          pageInfo{
          hasNextPage
          startCursor
          endCursor
          }
          nodes {
            location { ... on VulnerabilityLocationSecretDetection { blobPath,vulnerableClass,vulnerableMethod,file,startLine,endLine }}
            description
            confirmedAt
            confirmedBy {
              id
            }
            #pageInfo
            description
            descriptionHtml
            #details
            detectedAt
            discussions {
              edges {
                node {
                  id
                }
              }
            }
            dismissedAt
            dismissedBy {
              id
            }
            externalIssueLinks {
              edges {
                node {
                  id
                }
              }
            }
            falsePositive
            hasSolutions
            id
            identifiers {
              externalId
              externalType
              name
              url
            }
            links {
              name
            }
            mergeRequest {
              id
            }
            message
            notes {
              edges {
                node {
                  id
                }
              }
            }
            primaryIdentifier {
              externalId
              externalType
              name
              url
            }
            project {
              id
            }
            reportType
            resolvedAt
            resolvedBy {
              id
            }
            resolvedOnDefaultBranch
            scanner {
              id
              name
            }
            severity
            state
            title
            userNotesCount
            userPermissions {
              adminVulnerabilityIssueLink
            }
            vulnerabilityPath
            id
          }
        }
      }}""" % (project_name)
      # % (project_name, endCursor)
    elif report_type == "DEPENDENCY_SCANNING":
      query = """{
      project(fullPath: "%s") {
        vulnerabilities(reportType: DEPENDENCY_SCANNING, after : "", first : 1) {
          pageInfo{
          hasNextPage
          startCursor
          endCursor
          }
          nodes {
            location { 
                ... on VulnerabilityLocationDependencyScanning { blobPath,file,dependency {version}}
                }

            description
            confirmedAt
            confirmedBy {
              id
            }
            #pageInfo
            description
            descriptionHtml
            #details
            detectedAt
            discussions {
              edges {
                node {
                  id
                }
              }
            }
            dismissedAt
            dismissedBy {
              id
            }
            externalIssueLinks {
              edges {
                node {
                  id
                }
              }
            }
            falsePositive
            hasSolutions
            id
            identifiers {
              externalId
              externalType
              name
              url
            }
            links {
              name
            }
            mergeRequest {
              id
            }
            message
            notes {
              edges {
                node {
                  id
                }
              }
            }
            primaryIdentifier {
              externalId
              externalType
              name
              url
            }
            project {
              id
            }
            reportType
            resolvedAt
            resolvedBy {
              id
            }
            resolvedOnDefaultBranch
            scanner {
              id
              name
            }
            severity
            state
            title
            userNotesCount
            userPermissions {
              adminVulnerabilityIssueLink
            }
            vulnerabilityPath
            id
          }
        }
      }}""" % (project_name)
      # % (project_name, endCursor)
    elif report_type == "COVERAGE_FUZZING":
      query = """{
      project(fullPath: "%s") {
        vulnerabilities(reportType: COVERAGE_FUZZING, after : "", first : 1) {
          pageInfo{
          hasNextPage
          startCursor
          endCursor
          }
          nodes {
            location {                 
                ... on VulnerabilityLocationCoverageFuzzing { blobPath,vulnerableClass,vulnerableMethod,endLine,startLine,file,vulnerableClass}
                }

            description
            confirmedAt
            confirmedBy {
              id
            }
            #pageInfo
            description
            descriptionHtml
            #details
            detectedAt
            discussions {
              edges {
                node {
                  id
                }
              }
            }
            dismissedAt
            dismissedBy {
              id
            }
            externalIssueLinks {
              edges {
                node {
                  id
                }
              }
            }
            falsePositive
            hasSolutions
            id
            identifiers {
              externalId
              externalType
              name
              url
            }
            links {
              name
            }
            mergeRequest {
              id
            }
            message
            notes {
              edges {
                node {
                  id
                }
              }
            }
            primaryIdentifier {
              externalId
              externalType
              name
              url
            }
            project {
              id
            }
            reportType
            resolvedAt
            resolvedBy {
              id
            }
            resolvedOnDefaultBranch
            scanner {
              id
              name
            }
            severity
            state
            title
            userNotesCount
            userPermissions {
              adminVulnerabilityIssueLink
            }
            vulnerabilityPath
            id
          }
        }
      }}""" % (project_name)
      # % (project_name, endCursor)
    elif report_type == "CONTAINER_SCANNING":
      query = """{
      project(fullPath: "%s") {
        vulnerabilities(reportType: CONTAINER_SCANNING, after : "", first : 1) {
          pageInfo{
          hasNextPage
          startCursor
          endCursor
          }
          nodes {
            location { 
                ... on VulnerabilityLocationDependencyScanning { blobPath,file,dependency {version}}
                }

            description
            confirmedAt
            confirmedBy {
              id
            }
            #pageInfo
            description
            descriptionHtml
            #details
            detectedAt
            discussions {
              edges {
                node {
                  id
                }
              }
            }
            dismissedAt
            dismissedBy {
              id
            }
            externalIssueLinks {
              edges {
                node {
                  id
                }
              }
            }
            falsePositive
            hasSolutions
            id
            identifiers {
              externalId
              externalType
              name
              url
            }
            links {
              name
            }
            mergeRequest {
              id
            }
            message
            notes {
              edges {
                node {
                  id
                }
              }
            }
            primaryIdentifier {
              externalId
              externalType
              name
              url
            }
            project {
              id
            }
            reportType
            resolvedAt
            resolvedBy {
              id
            }
            resolvedOnDefaultBranch
            scanner {
              id
              name
            }
            severity
            state
            title
            userNotesCount
            userPermissions {
              adminVulnerabilityIssueLink
            }
            vulnerabilityPath
            id
          }
        }
      }}""" % (project_name)
      # % (project_name, endCursor)
    return query