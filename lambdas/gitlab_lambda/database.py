import boto3
from botocore.exceptions import ClientError
import json
import os
from aws_lambda_powertools import Logger
from datetime import datetime  
from datetime import timedelta
import pymysql


logger = Logger(service="Jira-integration")
database_host = os.environ['MySQLEndpoint']
db_user_name = os.environ['MySQLUserName']
db_password = os.environ['MySQLPassword']
db_name = os.environ['MySQLDbName']

class DatabaseManager:
    
    def __init__(self):
        logger.debug({"init database function"})
        self.rows = {}
        self.conn = []
    
    # Establish MYSQL Database connection
    def database_connection(self):
      
        try:
          self.conn = pymysql.connect(host=database_host, user=db_user_name, passwd=db_password, db=db_name, connect_timeout=5)
        except pymysql.MySQLError as e:
          logger.error("ERROR: Unexpected error: Could not connect to MySQL instance.")
          logger.error(e)
          sys.exit()
      
        logger.info("SUCCESS: Connection to RDS MySQL instance succeeded")
        
    # Select data from the MYSQL database        
    def select_data(self,tablename):
        try:
            cursor_object = self.conn.cursor(pymysql.cursors.DictCursor)
            sql_query = "select * from " + tablename
            cursor_object.execute(sql_query)
            
            if cursor_object.rowcount == 0 :
                self.rows[tablename] = []
            else:
                self.rows[tablename] = cursor_object.fetchall()
            logger.debug({"Database response from tablename  ": self.rows[tablename]})
            self.conn.commit()
            cursor_object.close()
            # self.conn.close()
        except Exception as e:
            logger.error({"Database query error ": e})
    
    def close_dbcon(self):
        
        try:
            self.conn.close()
        except Exception as e:
            logger.error({"error in close connection ": e})
    
    # Fetch data from the cache for particular project name/vulnarability ID
    def getdata_from_cache(self, tablename, data):
        logger.debug({"Database getdata_from_cache  ": self.rows})
        if tablename == "Gitlab_id":
            query_parameter = "Vulnerability_id"
        elif tablename == "Jira_Fields":
            query_parameter = "Project"
            
        for row in self.rows[tablename]:
            if row[query_parameter] == data:
                return row
        return {}