import boto3
from botocore.exceptions import ClientError
import json
import os
from aws_lambda_powertools import Logger

logger = Logger(service="Jira-integration")
queue_url = 'jira-integration.fifo'

class SQSManager:
    
    def __init__(self):
        logger.debug({"init SQS function"})
        self.sqs = boto3.client('sqs') 

    # Send message to the SQS Queue
    def send_msg(self, jira_data, msg_id):
        
        logger.debug({"msg_id":msg_id})
        
        try :
            response = self.sqs.send_message(
                    QueueUrl=queue_url,
                    MessageBody=json.dumps(jira_data),
                    MessageGroupId=msg_id,
                    MessageDeduplicationId= msg_id
            )
            return response
        except Exception as Error:
            logger.error({"Error in sending message to SQS queue": Error})
            return 